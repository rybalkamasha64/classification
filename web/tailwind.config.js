module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {
      colors: {
        'main-orange': '#E28B00',
        'push-orange': '#a16400',
        'background': '#000000',
        'new-blue': '#0a3248',
      },

    },
  },
  mode: 'jit',
  plugins: [
    require('@vueform/slider/tailwind'),
  ],
  screens: {
    'sm': '640px',
    // => @media (min-width: 640px) { ... }

    'md': '768px',
    // => @media (min-width: 768px) { ... }

    'lg': '1024px',
    // => @media (min-width: 1024px) { ... }

    'xl': '1280px',
    // => @media (min-width: 1280px) { ... }

    // eslint-disable-next-line comma-dangle
    '2xl': '1440px',
    // => @media (min-width: 1536px) { ... }
  }
}
