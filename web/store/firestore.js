import {initializeApp} from "firebase/app";
import {
  addDoc,
  collection,
  deleteDoc,
  deleteField,
  doc,
  getDocs,
  getFirestore,
  query,
  updateDoc,
  where
} from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCdZhGo9I9aNts0GZQ7DZqg2Nmmad3zXpo",
  authDomain: "classificator-e9f40.firebaseapp.com",
  projectId: "classificator-e9f40",
  storageBucket: "classificator-e9f40.appspot.com",
  messagingSenderId: "346573180744",
  appId: "1:346573180744:web:2784ade2f00cbcd481ed7d"
};
console.log('fetch');

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export async function getClasses() {
  const classesCol = collection(db, 'classes');
  const classesSnapshot = await getDocs(classesCol).catch((reason) => {
    console.log('Getting list of classes has stopped because of ' + reason)
  });
  return classesSnapshot.docs.map(doc => {
    return {
      id: doc.id,
      name: doc.data().name,
    }
  });
}

export async function addClass(name) {
  const ref = await addDoc(collection(db, "classes"), {
    name: name
  }).catch((reason) => {
    console.log('Adding has stopped because of ' + reason)
  });
  return ref.id;
}

export async function deleteClass(id) {
  const atrsCol = collection(db, 'classes', id, 'attributes');
  const attrsSnapshot = await getDocs(atrsCol).catch((reason) => {
    console.log('Getting list of attributes has stopped because of ' + reason)
  });
  Promise.all(attrsSnapshot.docs.map(attr => {
    deleteDoc(doc(db, "classes", id, "attributes", attr.id));
  })).catch((reason) => {
    console.log('Deleting list of attributes has stopped because of ' + reason)
  });

  return await deleteDoc(doc(db, "classes", id)).catch((reason) => {
    console.log('Deleting of class has stopped because of ' + reason)
  });
}

export async function updateAttributesValuesForClass(classId, payload) {
  const atrsCol = collection(db, 'classes', classId, 'attributes');
  let attrsSnapshot = await getDocs(atrsCol).catch((reason) => {
    console.log('Getting list of attributes has stopped because of ' + reason);
    return false;
  });
  attrsSnapshot.docs.map(async (attribute) => {
    let newAttr = payload.find(el => el.parentId === attribute.id);
    if (newAttr) {
      delete newAttr.parentId;
      await updateDoc(doc(db, 'classes', classId, 'attributes', attribute.id), newAttr).catch((reason) => {
        console.log('Updating of attribute has stopped because of ' + reason);
        return false;
      });
    }
  });
  return true;
}

export async function updateAttributesForClass(classId, payload) {
  const atrsCol = collection(db, 'classes', classId, 'attributes');
  const attrsSnapshot = await getDocs(atrsCol).catch((reason) => {
    console.log('Getting list of attributes has stopped because of ' + reason);
    return false;
  });
  Promise.all(attrsSnapshot.docs.map(attr => {
    deleteDoc(doc(db, "classes", classId, "attributes", attr.id));
  })).catch((reason) => {
    console.log('Deleting list of attributes has stopped because of ' + reason);
    return false;
  });

  Promise.all(payload.map(attr => {
    return addDoc(collection(db, "classes", classId, 'attributes'), attr).catch((reason) => {
      console.log('Adding has stopped because of ' + reason);
      return false;
    });
  }));
  return true;
}

export async function getAttributes() {
  const attrCol = collection(db, 'attributes');
  const attrSnapshot = await getDocs(attrCol).catch((reason) => {
    console.log('Getting list of attributes has stopped because of ' + reason)
  });
  return attrSnapshot.docs.map(doc => {
    return {
      id: doc.id,
      ...doc.data(),
    }
  });
}

export async function addAttribute(name) {
  const ref = await addDoc(collection(db, "attributes"), {
    name: name,
  }).catch((reason) => {
    console.log('Adding of attribute has stopped because of ' + reason)
  });
  return ref.id;
}

export async function updateAttribute(data) {
  const {id, ...payload} = data;
  const ref = doc(db, 'attributes', id);
  await updateDoc(ref, payload).catch((reason) => {
    console.log('Updating of attribute has stopped because of ' + reason)
    return -1;
  });
  if (payload.range) {
    await updateDoc(ref, {
      values: deleteField(),
    }).catch((reason) => {
      console.log('Updating of attribute has stopped because of ' + reason)
      return -1;
    });
  } else if (payload.values) {
    await updateDoc(ref, {
      range: deleteField(),
      meas: deleteField(),
    }).catch((reason) => {
      console.log('Updating of attribute has stopped because of ' + reason)
      return -1;
    });
  } else {
    await updateDoc(ref, {
      values: deleteField(),
      range: deleteField(),
      meas: deleteField(),
    }).catch((reason) => {
      console.log('Updating of attribute has stopped because of ' + reason)
      return -1;
    });
  }

  await updateAttributeForClasses(id, payload);
  return ref.id;
}

async function updateAttributeForClasses(id, payload) {
  const classes = await getClasses();
  classes.map(async el => {
    const q = query(collection(db, "classes", el.id, "attributes"), where("id", "==", id));
    const querySnapshot = await getDocs(q);
    querySnapshot.docs.map(async d => {
      await updateDoc(doc(db, 'classes', el.id, "attributes", d.id), payload).catch((reason) => {
        console.log('Deleting of attribute has stopped because of ' + reason)
      });
      if (payload.range) {
        await updateDoc(doc(db, 'classes', el.id, "attributes", d.id), {
          values: deleteField(),
        }).catch((reason) => {
          console.log('Updating of attribute has stopped because of ' + reason)
          return -1;
        });
      } else if (payload.values) {
        await updateDoc(doc(db, 'classes', el.id, "attributes", d.id), {
          range: deleteField(),
          meas: deleteField(),
        }).catch((reason) => {
          console.log('Updating of attribute has stopped because of ' + reason)
          return -1;
        });
      } else {
        await updateDoc(doc(db, 'classes', el.id, "attributes", d.id), {
          values: deleteField(),
          range: deleteField(),
          meas: deleteField(),
        }).catch((reason) => {
          console.log('Updating of attribute has stopped because of ' + reason)
          return -1;
        });
      }
    })
  })
}

export async function deleteAttribute(id) {
  await deleteDoc(doc(db, "attributes", id)).catch((reason) => {
    console.log('Deleting of attribute has stopped because of ' + reason)
  });
  const classes = await getClasses();
  classes.map(async el => {
    const q = query(collection(db, "classes", el.id, "attributes"), where("id", "==", id));
    const querySnapshot = await getDocs(q);
    querySnapshot.docs.map(async d => {
        await deleteDoc(doc(db, "classes", el.id, "attributes", d.id)).catch((reason) => {
          console.log('Deleting of attribute has stopped because of ' + reason)
        });
      }
    )
  })
}

export async function getTypes() {
  const typesCol = collection(db, 'types');
  const typesSnapshot = await getDocs(typesCol).catch((reason) => {
    console.log('Getting list of attributes has stopped because of ' + reason)
  });
  return typesSnapshot.docs.map(doc => {
    return {
      id: doc.id,
      name: doc.data().name,
      val: doc.data().val
    }
  });
}

export async function getClassAttributes(id) {
  const classesAtrCol = collection(db, 'classes', id, 'attributes');
  const classesAtrSnapshot = await getDocs(classesAtrCol).catch((reason) => {
    console.log('Getting list of class attributes has stopped because of ' + reason)
  });
  return classesAtrSnapshot.docs.map(doc => {
    return {
      parentId: doc.id,
      ...doc.data()
    };
  });
}
